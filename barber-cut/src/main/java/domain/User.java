package domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity 

public class User {
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private Long id;

@NotNull
@Size (min=5, message="Name must be atleast 5 characters long")
private String lastName;

@NotNull
@Size (min=5, message="Name must be atleast 5 characters long")
private String cut;
@NotNull
@Size (min=5, message="Name must be atleast 5 characters long")
private int phoneNumber;
@NotNull
@Size (min=5, message="Name must be atleast 5 characters long")
private Date theDate;
@NotNull
@Size (min=5, message="Name must be atleast 5 characters long")
private Date theTime ;



}
