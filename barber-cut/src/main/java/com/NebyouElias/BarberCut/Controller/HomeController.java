package com.NebyouElias.BarberCut.Controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
	@GetMapping("/index")
	public String home() {
		return "index";
	}
	
	@GetMapping("/barbers")
	public String barbers() {
		return "barbers";
	}

	/*@GetMapping("/booking")
	public String booking() {
		return "booking";
	}*/

	@GetMapping("/contact")
	public String contact() {
		return "contact";
	}

	@GetMapping("/elements")
	public String elements() {
		return "elements";
	}
	@GetMapping("/services")
	public String services() {
		return "services";
	}


	@GetMapping("/gallery")
	public String gallery() {
		return "gallery";
	}



}
