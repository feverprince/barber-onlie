package com.NebyouElias.BarberCut;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BarberCutApplication {

	public static void main(String[] args) {
		SpringApplication.run(BarberCutApplication.class, args);
	}

}

